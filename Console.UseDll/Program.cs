﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.IO;

namespace UseDll
{
    class Program
    {
        static void Main(string[] args)
        {
            //LocalMemoryAlloc();
            //TestLocalMemoryAllocAndFree(); 
            //new GoLibraryInvoke().GetGolangStringByDirectRead(); //正常运行
            new GoLibraryInvoke().GetGoCString();
            Console.Write("press any key to continue...");
            Console.ReadKey();
        }

        static void CLibraryInvokes()
        {
            //int total = sum(1, 2);
            //int value = subtract(5,1);
            //Console.WriteLine(total);
            //Console.WriteLine(value);

            CLibraryInvoke.SayHello();
            //ChangeAppIcon();
        }

        static void StackAlloc()
        {
            unsafe
            {
                var stackMemory = stackalloc byte[100];
            }
        }

        /// <summary>
        /// 测试指针和内存分配问题
        /// </summary>
        static void LocalMemoryAlloc()
        {
            //参考资料
            //https://learn.microsoft.com/zh-cn/archive/msdn-magazine/2018/january/csharp-all-about-span-exploring-a-new-net-mainstay#spant-%E5%92%8C-memoryt-%E5%A6%82%E4%BD%95%E4%B8%8E-net-%E5%BA%93%E9%9B%86%E6%88%90
            //C# 指针看看资料， http://wjhsh.net/2Yous-p-4887918.html

            IntPtr intPtr = Marshal.AllocHGlobal(2);
            Console.WriteLine(intPtr.ToInt64());
            unsafe
            {
                void* pointer = intPtr.ToPointer();
                byte* pointer_1= (byte*)pointer;
                byte* pointer_2 = (byte*)intPtr;

                //Console.WriteLine(*pointer);
                //Console.WriteLine(pointer_1);
                //Console.WriteLine(pointer_2);

                byte[] buffer = new byte[2] {3,2};
                fixed(byte* pointer_3 = &buffer[0])
                {
                    Console.WriteLine("指针长度：" + IntPtr.Size); //使用IntPtr.Size静态属性判断指针的长度类型
                    int address = (int)pointer_3;
                    byte* pointer_4= pointer_3 + 1;
                    
                    Console.WriteLine(address);
                    Console.WriteLine(*pointer_4); //使用指针间接寻址运算符获取指针地址表示的值
                }
            }
            try
            {
                //Span<byte> span = new Span<byte>();
                
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
            finally
            {

               Marshal.FreeHGlobal(intPtr);
            }
        }

        /// <summary>
        /// 测试内存的申请和释放
        /// </summary>
        static void TestLocalMemoryAllocAndFree()
        {
            IntPtr intPtr = Marshal.AllocHGlobal(2);
            Console.WriteLine(intPtr.ToInt64());
            unsafe
            {
                Span<byte> span = new Span<byte>(intPtr.ToPointer(), 2);
                span[0] = 1;
                span[1] = 2;
            }
            Marshal.FreeHGlobal(intPtr);
        }
    }
}
