﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UseDll
{
    internal class CLibraryInvoke
    {
        [DllImport("CDemoDll.dll", EntryPoint = "Sum", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int sum(int a, int b);

        [DllImport("CDemoDll.dll", EntryPoint = "sub", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int subtract(int a, int b);

        [DllImport("CDemoDll.dll", EntryPoint = "sayHello", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int SayHello();

        [DllImport("CDemoDll.dll",
            EntryPoint = "ChangeWinAppIcon", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl,
            SetLastError = true)]
        public static extern int ChangeWinAppIcon(string fileName, string iconFileName);

        private static void ChangeAppIcon()
        {
            //string winAppName = "Jcex.WMS.OnlineInstaller(电信版)v2.exe";
            //string iconName = @"D:\Pictures\My Pictures\常用图标\ico\07.ico";
            //ChangeWinAppIcon(winAppName, iconName);
            //最终解决方案参考以下链接地址上的说明
            //http://angusj.com/resourcehacker/
        }

        //static void CsharpAesEncrypt()
        //{
        //    string plainText;
        //    byte[] Key = new byte[] 
        //    {0x01,0x23,0x45,0x67,
        //     0x89,0xab,0xcd,0xef,
        //     0xfe,0xdc,0xba,0x98,
        //     0x76,0x54,0x32,0x10 };
        //    byte[] IV = Key;

        //    using (Aes aesAlg = Aes.Create())
        //    {
        //        aesAlg.Key = Key;
        //        aesAlg.IV = IV;
        //        ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
        //        using (MemoryStream msEncrypt = new MemoryStream())
        //        {
        //            using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
        //            {
        //                using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
        //                {
        //                    //Write all data to the stream.
        //                    swEncrypt.Write(plainText);
        //                }
        //                encrypted = msEncrypt.ToArray();
        //            }
        //        }

        //    }

        //}
    }
}
