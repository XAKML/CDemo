﻿using System.Text;

namespace PerformanceTesting
{
    public struct GoString
    {
        public IntPtr p;
        public int n;

        /// <summary>
        /// 获取C#语言的字符串内容
        /// </summary>
        /// <returns></returns>
        public string GetString()
        {
            if (p == IntPtr.Zero)
                return string.Empty;
            else
            {
                unsafe
                {
                    string rtnValue = new string((sbyte*)p.ToPointer(), 0, n, Encoding.UTF8);
                    return rtnValue;
                }
            }
        }
    }
}
