﻿using BenchmarkDotNet.Running;

namespace PerformanceTesting
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region 性能测试脚本
            //BenchmarkRunner.Run<TestContext>();
            //BenchmarkRunner.Run<CGoStringWrapper>();
            #endregion

            //new CGoStringWrapper().IntegerTypeMemoryLength();
            new CGoStringWrapper().TestGetChineseString(); //测试在go dll中返回字符串（包含中文）
            new CGoStringWrapper().TestInputGoString(); //测试在C#中输入字符串，
            //Console.ReadLine();
            //string value = new CGoStringWrapper().GetGolangStringByMemoryCopy();
            // Console.WriteLine(value);
        }
    }
}