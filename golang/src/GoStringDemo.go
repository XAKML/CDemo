package main

/*
#include <stdlib.h>
*/
import "C"
import (
	"fmt"
	"unsafe"
)

func main() {
	var name string

	fmt.Println("hello world")
	fmt.Scanln(&name)
	fmt.Println(name)
}

//export GetString
func GetString() string {
	rtnValue := "goland values"
	return rtnValue
}

//export GetCString
func GetCString() *C.char {
	gostr := "hello golang"
	cString := C.CString(gostr)
	//defer C.free(unsafe.Pointer(cString))
	return cString
}

//export FreeCString
func FreeCString(ptr *C.char) {
	C.free(unsafe.Pointer(ptr))
}
