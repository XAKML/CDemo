package main

/*
#include <stdlib.h>
*/
import "C"
import (
	"fmt"
	"reflect"
	"unsafe"
)

func main() {
	var name string
	VarLength()
	fmt.Println("hello world")
	fmt.Scanln(&name)
	fmt.Println(name)
}

//export GetString
func GetString() string {
	rtnValue := "goland values"
	return rtnValue
}

//export GetCString
func GetCString() *C.char {
	gostr := "hello golang"
	cString := C.CString(gostr)
	//defer C.free(unsafe.Pointer(cString))
	return cString
}

//export FreeCString
func FreeCString(ptr *C.char) {
	C.free(unsafe.Pointer(ptr))
}

// VarLength @title 测试文件名称
// @description  测试变量占用空间
//
//export VarLength
func VarLength() {
	var intA = 10
	var longA int64
	longA = 90
	fmt.Printf("%s%d\n", "整型变量内存长度：", unsafe.Sizeof(intA))
	fmt.Printf("%s", reflect.TypeOf(intA))
	fmt.Printf("%s%d\n", "长整型变量内存长度：", unsafe.Sizeof(longA))
}

// GetChineseString 返回中文字符串，在Csharp中调用
// @return 中文字符串
//
//export GetChineseString
func GetChineseString() string {
	chineseString := "中国， 你好！"
	return chineseString
}

// InputGoString 测试外部调用输入字符串
//
//export InputGoString
func InputGoString(str *C.char) *C.char {
	rtnValue := "received content:" + C.GoString(str)
	return C.CString(rtnValue)
}

/**
environment				CGO_ENABLED=1;GOOS=windows;GOARCH=amd64
go tool arguments		--buildmode=c-shared -o C:\Users\jcex\GolandProjects\demos\bin\golang_demo.dll

run kind=package

**/

/**
参考资料： 
https://www.cnblogs.com/timeddd/p/11731160.html
https://blog.csdn.net/nicholasleo/article/details/109725797
http://www.manongjc.com/detail/52-gdkrjeqxfqvbkpa.html
https://www.cnblogs.com/jiftle/p/12817334.html
https://www.cnblogs.com/timeddd/p/11731160.html

golang
字符串拼接： https://blog.csdn.net/qq_38228830/article/details/109239260

关于IntPtr类型和AllocHGlobal函数的使用；
https://blog.csdn.net/YakshaBlade/article/details/105447423

IntPtr 结构
https://learn.microsoft.com/zh-cn/dotnet/api/system.intptr?view=netframework-4.8

**/

